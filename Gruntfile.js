module.exports = function(grunt) {
	grunt.initConfig({
		clean: {
	        all: ['dist/'],
	        temp: ['dist/temp/']
	    },

	    jshint: {
	        dist: {
	            src: ['src/js/app/**/*.js']
	        }
	    },

	    concat: {

            scripts: {
                src: 'src/js/app/**/*.js',
                dest: 'dist/temp/js/scripts.js'
            },

            vendor: {
                src: [
                  'src/js/vendor/jQuery/*.js',
                ],
                dest: 'dist/temp/js/vendor.js'
            },

            all: {
                src: ['dist/temp/js/vendor.min,js', 'dist/temp/js/scripts.min.js'],
                dest: 'dist/js/scripts.js'
            }

        },

        uglify: {
            scripts: {
                src: ['dist/temp/js/scripts.js'],
                dest: 'dist/temp/js/scripts.min.js'
            },
            vendor: {
                src: ['dist/temp/js/vendor.js'],
                dest: 'dist/temp/js/vendor.min.js'
            }
        },

        cssmin: {
            all: {
                src: [
                  'dist/temp/css/main.css'
                ],
                dest: 'dist/css/styles.min.css'
            }
        },

        sass: {
	      dist: {
	        files: {
	          'dist/temp/css/main.css': 'src/css/main.scss',
	        }
	      }
	    },

	    watch: {
	        src: {
	          files: ['src/js/app/**/*.js', 'src/css/**/*.scss'],
	          tasks: ['default'],
	        },
	      },

    });

	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task(s).
	grunt.registerTask('default', ['jshint', 'clean:all', 'concat:scripts', 'concat:vendor', 'uglify:scripts', 'uglify:vendor', 'sass', 'cssmin', 'concat:all', 'clean:temp', 'watch']);
};